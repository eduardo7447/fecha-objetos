package cal;

public class Fecha {
	
	public int dia;
	public int mes;
	public int anio;
	
	public Fecha() {
		this.dia = 1;
		this.mes = 1;
		this.anio = 1970;
	}
	public Fecha(int dia, int mes) {
		this.dia = 0;
		this.mes = 0;
		
		if (!esValida()) {
			throw new IllegalArgumentException("fecha inválida: " + this.dia);
		}
	}

	public Fecha(int dia, int mes, int anio) {
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		
		if (!esValida()) {
			throw new IllegalArgumentException("fecha inválida: " + this.dia);
		}
	}

	public void imprimir() {
		System.out.println(this.dia + " /" + this.mes + " /" + this.anio);
	}

//	public String toString() {
//		return dia + "/" + mes + "/" + anio;
//	}

	public static boolean esBisiesto(int anio) {
		return anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0;
	}

	public static int diasDelMes(int mes, int anio) {
		if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			return 30;
		}
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
			return 31;
		}
		if ((mes == 2) && esBisiesto(anio)) {
			return 29;
		}
		return 28;
	}

	public boolean esValida() {		
	
		  if ((this.mes>0 && this.mes <13) && ((this.mes == 4 || this.mes == 6 || this.mes == 9 || this.mes == 11) && (this.dia > 0 && this.dia<=30)) )  { 
		   return true; 
		  } 
		  if (( (this.mes>0 && this.mes <13) && (this.mes == 1 || this.mes == 3 || this.mes == 5 || this.mes == 7 || this.mes == 8 || this.mes == 10 || this.mes == 12) &&(this.dia > 0 && this.dia <= 31))  ){ 
		   return true; 
		  }  
		   if ( this.mes == 2 && esBisiesto(this.anio) && ( this.dia > 0 && this.dia <= 29)) {
			   return true;
		   }
		   if ( (this.mes==2 ) && ! esBisiesto(this.anio) &&( this.dia > 0 && dia <=28)) { 
		    return true;
		   } 
		
		return false;
	}

	public  void avanzarDia() {
		this.dia++;
		if(this.esValida() ) {
			
			return;
		}
		this.dia = 1;
	}

	public int diaDelAnio() {
		int aux = 0;
		for (int i = 1; i < this.mes; i++) {
			aux = aux + diasDelMes(i, this.anio);
		}
		aux = aux + this.dia;
		return aux;
	}

	// TODO
	public  boolean antesQue(Fecha otra) {
		if (this.mes < otra.mes && this.anio < otra.anio) {
		return true;
		}else {
			if (this.mes == otra.mes && this.anio < otra.anio || this.mes < otra.mes && this.anio == otra.anio || this.mes > otra.mes && this.anio < otra.anio ) {
				return true;
			}else {
				if(this.dia < otra.dia && this.mes == otra.mes && this.anio == otra.anio){
					return true;
				}
			}
		}
		return false;
	}
	public void mostrar() {
		System.out.println( this.dia + " /" + this.mes+ " /"+ this.anio);
	}
	public int diasDeDiferenciaCon(Fecha otra) {
		int dias = 0 ;
		if (this.antesQue(otra)) {
			for (int i = this.anio; i< otra.anio ;i++  ) {
				if(esBisiesto(i)) {
					dias = dias + 366;
				}
				dias+= 365;
			}
			dias = dias +(otra.diaDelAnio() - this.diaDelAnio());
		}
	
	return dias;
	}
	// Esto se suele usar sólo para probar cosas,
	// una vez que ven que todo funca bien, bórrenlo.
	public static void main(String[] args) {
		//Fecha f1 = new Fecha();
		Fecha f2 = new Fecha(30, 6,2021);
		Fecha f3 = new Fecha(1, 7,2023);
//		f1.imprimir();
		//System.out.println(f2.antesQue(f3));
		System.out.println(f2.diasDeDiferenciaCon(f3));
//		f2.avanzarDia();
//		f2.mostrar();
//		f2.avanzarDia();
//		f2.mostrar();
	}

}
